import os

from connexion import App

from validation_workflow_api.utils import ApiMarshmallowIOPreprocessor


def create_app():

    app = App("validation-workflow-api")
    spec = ApiMarshmallowIOPreprocessor().read_specification(
        os.path.join(os.path.dirname(__file__), "specification/api.yaml")
    )
    api = app.add_api(spec)

    # app.before_request(middleware.before_request)

    # Build the API with namespace and resources.
    # api = Api(
    #     app,
    #     version="1.0.0",
    #     title="ValidationWorkflowApi",
    #     description="Integrates workflow engine (airflow) with kibana user interface.",
    # )
    # api_namespace = api.namespace("v1", description="Workflow integration API v1")
    # api_namespace.add_resource(ConfigGlobals, "/system")
    # api_namespace.add_resource(ConfigMetrics, "/system/metric")
    # api_namespace.add_resource(WorkflowTransformation, "/workflow/transformation")
    # api_namespace.add_resource(WorkflowValidation, "/workflow/validation")
    # api_namespace.add_resource(WorkflowStatusMetric, "/workflow/status")
    # api_namespace.add_resource(Ping, "/ping")

    # Build the docs
    # default_responses = {200: "OK", 400: "Validation error", 500: "Server Error"}
    # api.doc(
    #     responses=default_responses,
    #     params=build_params_dict(ConfigMetricsInputSchema()),
    # )(ConfigMetrics)
    # api.doc(
    #     responses=default_responses,
    #     params=build_params_dict(ConfigGlobalsInputSchema()),
    # )(ConfigGlobals)
    # api.doc(
    #     responses=default_responses,
    #     params=build_params_dict(WorkflowModifyInputSchema()),
    # )(WorkflowModify)
    # api.doc(
    #     responses=default_responses,
    #     params=build_params_dict(WorkflowValidateInputSchema()),
    # )(WorkflowValidate)
    # api.doc(
    #     responses=default_responses,
    #     params=build_params_dict(WorkflowTransformsInputSchema()),
    # )(WorkflowTransforms)
    # api.doc(
    #     responses=default_responses,
    #     params=build_params_dict(WorkflowStatusMetricInputSchema()),
    # )(WorkflowStatusMetric)
    # api.doc(responses={200: json.dumps(Ping.response)})(Ping)

    return app, api


application, api = create_app()
