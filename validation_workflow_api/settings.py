"""Settings that get initialized at startup based on environment variables."""

import os

debug = os.getenv("API_DEBUG") == "1"
