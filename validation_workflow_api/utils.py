import yaml
from marshmallow import fields as marshmallow_fields
from werkzeug.exceptions import BadRequest

from connexion.utils import get_function_from_name


def validate_io(input_schema, output_schema):
    def ret_fun(func):
        def validated_func(*args, **kwargs):
            if "body" in kwargs:
                kwargs = kwargs["body"]

            payload = input_schema.load(kwargs)
            if payload[1]:
                raise BadRequest(payload[1])

            response = func(*args, payload[0])
            response_json = output_schema.dump(response)
            return response_json

        validated_func.input_schema = input_schema
        validated_func.output_schema = output_schema

        return validated_func

    return ret_fun


class ApiMarshmallowIOPreprocessor:
    def _generate_schema_field(self, field):
        if isinstance(field, marshmallow_fields.Nested):
            return self._generate_schema(field.schema)

        if isinstance(field, marshmallow_fields.List):
            type_attr = hasattr(field, "inner") or "container"
            return {
                "type": "array",
                "items": self._generate_schema_field(getattr(field, type_attr)),
            }

        if isinstance(field, marshmallow_fields.Dict):
            return {"type": "object"}

        supported = [
            marshmallow_fields.Number,
            marshmallow_fields.Integer,
            marshmallow_fields.Boolean,
        ]
        for s in supported:
            if isinstance(field, s):
                return {"type": type(field).__name__.lower()}

        # everything else has to be represented as string
        return {"type": "string"}

    def _generate_schema(self, schema):
        schema_spec = {
            "type": "object",
            "properties": {
                name: self._generate_schema_field(field)
                for name, field in schema.fields.items()
            },
        }

        if schema.many:
            return {"type": "array", "items": schema_spec}
        else:
            return schema_spec

    def read_specification(self, path):
        with open(path) as file:
            spec = yaml.safe_load(file)

            for path, path_block in spec["paths"].items():
                for method, method_block in path_block.items():
                    func = get_function_from_name(method_block["operationId"])

                    if hasattr(func, "input_schema"):
                        if method == "get":
                            method_block["parameters"] = [
                                {
                                    "in": "query",
                                    "name": name,
                                    "schema": self._generate_schema_field(field),
                                    "required": field.required,
                                    "description": field.metadata["description"],
                                }
                                for name, field in func.input_schema.fields.items()
                            ]

                        elif method == "post":
                            method_block["requestBody"] = {
                                "required": True,
                                "content": {
                                    "application/json": {
                                        "schema": self._generate_schema(
                                            func.input_schema
                                        )
                                    }
                                },
                            }

                    if hasattr(func, "output_schema"):
                        method_block["responses"][200]["content"] = {
                            "application/json": {
                                "schema": self._generate_schema(func.output_schema)
                            }
                        }

            return spec
