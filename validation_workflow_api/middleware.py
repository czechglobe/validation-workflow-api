"""Custom middleware for flask."""

import uuid

import flask
import structlog

log = structlog.get_logger()


def logger():
    """Return a structlog logger object.

    The logger will have a request ID and some extra info bound to it.
    """
    return log.new(request_id=str(uuid.uuid4()), url=flask.request.path)


def before_request():
    """Flask middleware handle for before each request."""
    flask.g.logger = logger()
