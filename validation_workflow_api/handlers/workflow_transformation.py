import portion as P
from marshmallow import Schema, fields, validates, ValidationError

from validation_workflow import settings
from validation_workflow.models.dag_model import DAGModel
from validation_workflow.models.transform_model import TransformModel
from validation_workflow.models.validation_model import ValidationModel
from validation_workflow.operators.transformations.utils import get_transform_list
from validation_workflow.utils import EDITABLE_STEPS
from validation_workflow.utils import iso_timestamp, idealize
from validation_workflow_api.handlers.workflow import WorkflowInputSchemaBase
from validation_workflow_api.utils import validate_io

validation_model = ValidationModel(settings.conn_id_es, settings.index_intervals_config)
transform_model = TransformModel(settings.conn_id_es, settings.index_intervals_config)
dag_model = DAGModel()


MODE_INTERACTIVE = "interactive"
MODE_BATCH = "batch"
ALL_MODES = [MODE_INTERACTIVE, MODE_BATCH]


class WorkflowTransformationGetInputSchema(WorkflowInputSchemaBase):
    pass


class WorkflowTransformationGetOutputSchema(Schema):
    start = fields.DateTime()
    end = fields.DateTime()
    transformation = fields.String()
    transformation_params = fields.Dict(keys=fields.String())


@validate_io(
    WorkflowTransformationGetInputSchema(),
    WorkflowTransformationGetOutputSchema(many=True),
)
def get(payload):
    location = payload["location"]
    step = payload["step"]
    metric = payload["metric"]
    start = iso_timestamp(payload["start"])
    end = iso_timestamp(payload["end"])

    interval = P.closedopen(start, end)

    intervals = transform_model.get_transform(location, step, metric, interval)

    aintervals = [
        (ainterval, settings)
        for interval, settings, in intervals.items()
        for ainterval in interval
    ]

    aintervals.sort(key=lambda a: a[0].lower)

    return [
        {"start": ainterval.lower, "end": ainterval.upper, **settings}
        for ainterval, settings in aintervals
    ]


class WorkflowTransformationPostInputSchema(WorkflowInputSchemaBase):
    transformation = fields.String(required=True, description="Transformation name")
    transformation_params = fields.Dict(
        required=True, description="Transformation parameters", keys=fields.String()
    )
    mode = fields.String(
        missing=MODE_BATCH,
        description="Mode of processing data: interactive - run task immediately and wait for finish, batch - leave it to airflow scheduler",
    )

    @validates("step")
    def validate_step(self, value):
        if value not in EDITABLE_STEPS:
            raise ValidationError(f"Step has to be one of {', '.join(EDITABLE_STEPS)}")

    @validates("transformation")
    def validate_transformation(self, value):
        if value not in get_transform_list():
            raise ValidationError(
                f"Transformation has to be one of {', '.join(get_transform_list())}"
            )

    @validates("mode")
    def validate_mode(self, value):
        if value not in ALL_MODES:
            raise ValidationError(f"Mode has to be one of {', '.join(ALL_MODES)}")


class WorkflowTransformationPostOutputSchema(Schema):
    success = fields.Boolean()


@validate_io(
    WorkflowTransformationPostInputSchema(many=True),
    WorkflowTransformationPostOutputSchema(),
)
def post(payload_entries):
    for payload in payload_entries:
        location = payload["location"]
        step = payload["step"]
        metric = payload["metric"]
        start = iso_timestamp(payload["start"])
        end = iso_timestamp(payload["end"])
        transformation = payload["transformation"]
        transformation_params = payload["transformation_params"]
        mode = payload.get("mode", MODE_INTERACTIVE)

        interval = P.closedopen(start, end)
        interval_settings = {
            "transformation": transformation,
            "transformation_params": transformation_params,
        }

        transform_model.set_transform(
            location, step, metric, interval, interval_settings
        )
        dag_model.clear_and_run_tasks(
            location=location,
            task_id=f"fix_{step}_data_{idealize(metric)}",
            interval=interval,
            interactive=mode == MODE_INTERACTIVE,
        )

    return {"success": True}
