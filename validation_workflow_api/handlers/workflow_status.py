import portion as P
from marshmallow import Schema, fields
from werkzeug.exceptions import BadRequest

from validation_workflow import settings
from validation_workflow.metrics_config import load_metrics_config
from validation_workflow.models.dag_model import DAGModel
from validation_workflow.models.validation_model import ValidationModel
from validation_workflow.utils import iso_timestamp
from validation_workflow_api.handlers.workflow import WorkflowInputSchemaBase

validation_model = ValidationModel(settings.conn_id_es, settings.index_intervals_config)
dag_model = DAGModel()
metrics_config = load_metrics_config()

VIEW_TYPE_VALIDATION = "validation"
VIEW_TYPE_PROCESSING = "processing"
VIEW_TYPES = [VIEW_TYPE_VALIDATION, VIEW_TYPE_PROCESSING]


class WorkflowStatusInputSchema(WorkflowInputSchemaBase):
    step = fields.String(
        required=False,
        description="ElasticSearch index name to identify step of data workflow",
    )
    metric = fields.String(required=False, description="Name of metric")


class WorkflowStatusOutputSchema(Schema):
    class IntervalStatusSchema(Schema):
        start = fields.DateTime()
        end = fields.DateTime()
        status = fields.String()

    raw = fields.List(fields.Nested(IntervalStatusSchema))
    unit = fields.List(fields.Nested(IntervalStatusSchema))
    final = fields.List(fields.Nested(IntervalStatusSchema))


def get_metrics(metric, metric_step, location):
    location_config = metrics_config.get_location_config(location)

    if metric or metric_step:
        if not (metric and metric_step):
            raise BadRequest("You have to provide both parameters metric and step")

        single_metric = next(
            filter(
                lambda m: getattr(m, f"name_{metric_step}") == metric,
                location_config.metrics,
            ),
            None,
        )

        return [single_metric]

    return location_config.metrics


def get_status(payload, view_type):
    location = payload["location"]
    metric = payload.get("metric")
    metric_step = payload.get("step")
    start = iso_timestamp(payload["start"])
    end = iso_timestamp(payload["end"])

    metrics = get_metrics(metric, metric_step, location)

    result = None
    interval = P.closedopen(start, end)

    if view_type == VIEW_TYPE_VALIDATION:
        result = validation_model.get_metric_status(location, interval, metrics)

    elif view_type == VIEW_TYPE_PROCESSING:
        result = dag_model.get_metric_status(location, interval, metrics)

    payload = {}
    for step, intervals in result.items():
        aintervals_statuses = [
            (ainterval, status)
            for interval, status in intervals.items()
            for ainterval in interval
        ]
        aintervals_statuses.sort(key=lambda x: x[0].lower)
        payload[step] = [
            {"start": ainterval.lower, "end": ainterval.upper, "status": status}
            for ainterval, status in aintervals_statuses
        ]
    return payload
