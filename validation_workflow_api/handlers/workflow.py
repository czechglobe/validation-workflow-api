from marshmallow import Schema, fields, validates, validates_schema, ValidationError

from validation_workflow.metrics_config import load_metrics_config
from validation_workflow.utils import ALL_STEPS


class WorkflowInputSchemaBase(Schema):
    metrics_config = load_metrics_config()

    location = fields.String(required=True, description="Id of location")
    step = fields.String(
        required=True,
        description="ElasticSearch index name to identify step of data workflow",
    )
    metric = fields.String(required=True, description="Name of metric")
    start = fields.DateTime(
        required=True, description="Start of interval (e.g. 2019-07-31T15:03:00)"
    )
    end = fields.DateTime(
        required=True, description="End of interval (e.g. 2019-09-20T15:03:00)"
    )

    @validates("step")
    def validate_step(self, value):
        if value not in ALL_STEPS:
            raise ValidationError(f"Step has to be one of {', '.join(ALL_STEPS)}")

    @validates_schema
    def validate_location(self, data, **kwargs):
        if data and data.get("location"):
            try:
                location_config = self.metrics_config.get_location_config(
                    data["location"]
                )
            except KeyError:
                raise ValidationError("Location does not exists", ["location"])

            if data.get("metric") and data.get("step"):
                single_metric = next(
                    filter(
                        lambda m: getattr(m, f"name_{data['step']}") == data["metric"],
                        location_config.metrics,
                    ),
                    None,
                )
                if not single_metric:
                    raise ValidationError(
                        "Metric with this name does not exists for this step",
                        ["metric"],
                    )

    @validates_schema
    def validate_start_end(self, data, **kwargs):
        if data and data.get("start") and data.get("end"):
            if not data["start"] < data["end"]:
                raise ValidationError(
                    "Start datetime has to be before end datetime", ["start", "end"]
                )
