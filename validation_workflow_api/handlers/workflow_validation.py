import portion as P
from marshmallow import Schema, fields, validates, ValidationError

from validation_workflow import settings
from validation_workflow.models.validation_model import ValidationModel
from validation_workflow.utils import EDITABLE_STEPS
from validation_workflow.utils import iso_timestamp
from validation_workflow_api.handlers.workflow import WorkflowInputSchemaBase
from validation_workflow_api.utils import validate_io

validation_model = ValidationModel(settings.conn_id_es, settings.index_intervals_config)


def get():
    pass  # TODO implement


class WorkflowValidationPostInputSchema(WorkflowInputSchemaBase):
    revoke = fields.Boolean(
        missing=False, description="Revoke validated status for interval"
    )

    @validates("step")
    def validate_mode(self, value):
        if value not in EDITABLE_STEPS:
            raise ValidationError(f"Step has to be one of {', '.join(EDITABLE_STEPS)}")


class WorkflowValidationPostOutputSchema(Schema):
    success = fields.Boolean()


@validate_io(
    WorkflowValidationPostInputSchema(many=True), WorkflowValidationPostOutputSchema()
)
def post(payload_entries):
    for payload in payload_entries:
        location = payload["location"]
        step = payload["step"]
        metric = payload["metric"]
        start = iso_timestamp(payload["start"])
        end = iso_timestamp(payload["end"])
        revoke = payload.get("revoke")

        interval = P.closedopen(start, end)

        validation_model.set_validated(location, step, [metric], interval, not revoke)

    return {"success": True}
