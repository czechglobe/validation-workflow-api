from marshmallow import Schema, fields

from validation_workflow_api.utils import validate_io


class PingInputSchema(Schema):
    pass


class PingOutputSchema(Schema):
    pong = fields.Boolean()


class Ping:
    """Return a response saying ``{"pong": true}``.

    This is useful for testing and health checks.
    """

    response = {"pong": True}

    @staticmethod
    @validate_io(PingInputSchema(), PingOutputSchema())
    def get(payload):
        return Ping.response
