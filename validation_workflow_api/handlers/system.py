from functools import lru_cache

from marshmallow import Schema, fields

from airflow.configuration import conf
from validation_workflow.metrics_config import load_metrics_config
from validation_workflow.operators.transformations.utils import (
    get_transform_list,
    get_transform_params,
)
from validation_workflow.utils import idealize
from validation_workflow_api.utils import validate_io


class SystemInputSchema(Schema):
    pass


class SystemOutputSchema(Schema):
    class LocationSchema(Schema):
        id = fields.String()
        title = fields.String()
        frequency = fields.String()

    class TransformSchema(Schema):
        class TransformParamSchema(Schema):
            name = fields.String()
            default = fields.Raw()
            type = fields.String()

        id = fields.String()
        name = fields.String()
        category = fields.String()
        description = fields.String()
        params = fields.List(fields.Nested(TransformParamSchema))

    airflow_base_url = fields.String()
    locations = fields.List(fields.Nested(LocationSchema))
    transforms = fields.List(fields.Nested(TransformSchema))


@lru_cache()
def get_airflow_base_url():
    return conf.get("webserver", "base_url")


@lru_cache()
def get_locations():
    metrics_config = load_metrics_config()
    locations = [
        {
            "id": idealize(location),
            "title": location,
            "frequency": f"{int(metrics_config.get_location_config(location).frequency.total_seconds())}s",
        }
        for location in metrics_config.get_locations()
    ]
    return locations


@lru_cache()
def get_transforms():
    transforms = get_transform_list()
    return [get_transform_params(transform) for transform in transforms]


@validate_io(SystemInputSchema(), SystemOutputSchema())
def get(payload):
    return {
        "airflow_base_url": get_airflow_base_url(),
        "locations": get_locations(),
        "transforms": get_transforms(),
    }
