from functools import lru_cache

from marshmallow import Schema, fields, validates, ValidationError

from validation_workflow.metrics_config import load_metrics_config
from validation_workflow.utils import ALL_STEPS
from validation_workflow_api.utils import validate_io


class SystemMetricInputSchema(Schema):
    step = fields.String(
        required=True,
        description="ElasticSearch index name to identify step of data workflow.",
    )

    @validates("step")
    def validate_step(self, value):
        if value not in ALL_STEPS:
            raise ValidationError(f"Step has to be one of {', '.join(ALL_STEPS)}")


class SystemMetricOutputSchema(Schema):
    class LocationSchema(Schema):
        id = fields.String()
        title = fields.String()
        frequency = fields.String()
        metrics = fields.List(fields.String())

    locations = fields.List(fields.Nested(LocationSchema))


@lru_cache()
def get_config_metrics(step):
    metrics_config = load_metrics_config()
    locations = [
        {
            "id": location_id,
            "title": metrics_config.get_location_config(location_id).name,
            "frequency": f"{int(metrics_config.get_location_config(location_id).frequency.total_seconds())}s",
            "metrics": [
                getattr(metric, f"name_{step}")
                for metric in metrics_config.get_location_config(location_id).metrics
                if getattr(metric, f"name_{step}")
            ],
        }
        for location_id in metrics_config.get_locations()
    ]
    return {"locations": locations}


@validate_io(SystemMetricInputSchema(), SystemMetricOutputSchema())
def get(payload):
    step = payload["step"]
    return get_config_metrics(step)
