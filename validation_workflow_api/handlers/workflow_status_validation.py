from validation_workflow_api.handlers.workflow_status import (
    WorkflowStatusInputSchema,
    WorkflowStatusOutputSchema,
    VIEW_TYPE_VALIDATION,
    get_status,
)
from validation_workflow_api.utils import validate_io


@validate_io(WorkflowStatusInputSchema(), WorkflowStatusOutputSchema())
def get(payload):
    return get_status(payload, view_type=VIEW_TYPE_VALIDATION)
