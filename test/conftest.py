import logging
import os

import pytest

from airflow import settings as airflow_settings
from airflow.models import Connection
from airflow.utils.db import merge_conn
from validation_workflow import settings
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook


def pytest_configure(config):
    settings.conn_id_es = "elasticsearch_test"
    settings.index_intervals_config = ".intervals-config"
    settings.config_path = os.path.join(os.path.dirname(__file__), "configs/main.yaml")

    if config.getoption("verbose") == 0:
        logger = logging.getLogger("airflow")
        logger.propagate = False
        logger = logging.getLogger("elasticsearch")
        logger.propagate = False


@pytest.fixture
def app():
    session = airflow_settings.Session()
    merge_conn(
        Connection(
            conn_id="elasticsearch_test",
            conn_type="http",
            host="test-elasticsearch",
            port=9200,
        ),
        session,
    )
    es_hook = ElasticsearchHook("elasticsearch_test")
    es_hook.create_index(
        ".intervals-config",
        {
            "mappings": {
                "properties": {
                    "fragments": {"type": "object", "enabled": False},
                    "key": {"type": "text", "analyzer": "keyword"},
                }
            }
        },
    )

    from validation_workflow_api.app import application

    yield application.app

    es_hook.delete_index(".intervals-config")


@pytest.fixture
def client(app):
    return app.test_client()
