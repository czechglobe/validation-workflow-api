from jsonschema import validate

from airflow.configuration import conf


def test_config_metrics_schema(client):
    schema = {
        "type": "object",
        "properties": {
            "locations": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "id": {"type": "string"},
                        "title": {"type": "string"},
                        "frequency": {"type": "string"},
                        "metrics": {"type": "array", "items": {"type": "string"}},
                    },
                    "required": ["id", "title", "frequency", "metrics"],
                },
            }
        },
        "required": ["locations"],
    }

    response = client.get("/v1/system/step?step=raw")
    assert response.status_code == 200
    validate(response.json, schema)


def test_config_metrics_data_raw(client):
    data = {
        "locations": [
            {
                "frequency": "3600s",
                "id": "torrentdepareis",
                "metrics": ["temp10", "hum10", "ws10"],
                "title": "TorrentDePareis",
            },
            {
                "frequency": "600s",
                "id": "bangkok",
                "metrics": ["co2"],
                "title": "Bangkok",
            },
        ]
    }

    response = client.get("/v1/system/step?step=raw")
    assert response.status_code == 200
    assert response.json == data


def test_config_metrics_data_unit(client):
    data = {
        "locations": [
            {
                "frequency": "3600s",
                "id": "torrentdepareis",
                "metrics": ["temperature", "humidity", "wind_speed"],
                "title": "TorrentDePareis",
            },
            {
                "frequency": "600s",
                "id": "bangkok",
                "metrics": ["so2"],
                "title": "Bangkok",
            },
        ]
    }

    response = client.get("/v1/system/step?step=unit")
    assert response.status_code == 200
    assert response.json == data


def test_config_globals_schema(client):
    schema = {
        "type": "object",
        "properties": {
            "airflow_base_url": {"type": "string"},
            "transforms": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "category": {"type": "string"},
                        "description": {"type": "string"},
                        "id": {"type": "string"},
                        "name": {"type": "string"},
                        "params": {"type": "array", "items": {"type": "object"}},
                    },
                    "required": ["category", "description", "id", "name", "params"],
                },
            },
            "locations": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "id": {"type": "string"},
                        "title": {"type": "string"},
                        "frequency": {"type": "string"},
                    },
                    "required": ["id", "title", "frequency"],
                },
            },
        },
        "required": ["airflow_base_url", "transforms"],
    }

    response = client.get("/v1/system")
    assert response.status_code == 200
    validate(response.json, schema)


def test_config_globals_airflow_base_url(client):
    conf.set("webserver", "base_url", "http://localhost:8080")
    response = client.get("/v1/system")
    assert response.status_code == 200
    assert response.json["airflow_base_url"] == "http://localhost:8080"
