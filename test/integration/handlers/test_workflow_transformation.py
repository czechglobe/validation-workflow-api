import pytest


def test_workflow_modify(client):
    expected_id = [
        {
            "end": "2020-02-01T00:00:00+00:00",
            "start": "2020-01-01T00:00:00+00:00",
            "transformation": "nop",
            "transformation_params": {},
        }
    ]

    response = client.get(
        "/v1/workflow/transformation?location=torrentdepareis&start=2020-01-01T00:00:00&end=2020-02-01T00:00:00&step=raw&metric=temp10"
    )
    assert response.status_code == 200
    assert response.json == expected_id

    response = client.post(
        "/v1/workflow/transformation",
        json=[
            {
                "location": "torrentdepareis",
                "start": "2020-01-01T00:00:00",
                "end": "2020-02-01T00:00:00",
                "step": "raw",
                "metric": "temp10",
                "transformation": "scale",
                "transformation_params": {"constant": 2.0},
                "mode": "batch",
            }
        ],
    )
    assert response.status_code == 200
    assert response.json == {"success": True}

    expected_scale = [
        {
            "end": "2020-02-01T00:00:00+00:00",
            "start": "2020-01-01T00:00:00+00:00",
            "transformation": "scale",
            "transformation_params": {"constant": 2.0},
        }
    ]

    response = client.get(
        "/v1/workflow/transformation?location=torrentdepareis&start=2020-01-01T00:00:00&end=2020-02-01T00:00:00&step=raw&metric=temp10"
    )
    assert response.status_code == 200
    assert response.json == expected_scale


@pytest.mark.parametrize(
    "incorrect_param",
    [
        {"location": "nonexistent"},
        {"start": "in-cor-ect da:te"},
        {"end": "in-cor-ect da:te"},
        {"step": "nonexistent"},
        {"metric": "nonexistent"},
        {"transformation": "nonexistent"},
        {"mode": "nonexistent"},
        {"transformation_params": "wrong type"},
    ],
)
def test_workflow_modify_incorrect(incorrect_param, client):
    params = {
        "location": "torrentdepareis",
        "start": "2020-01-01T00:00:00",
        "end": "2020-02-01T00:00:00",
        "step": "raw",
        "metric": "temp10",
        "transformation": "scale",
        "transformation_params": {"constant": 2.0},
        "mode": "batch",
    }

    params.update(incorrect_param)

    response = client.post("/v1/workflow/transformation", json=[params])
    assert response.status_code == 400
