import pytest
from jsonschema import validate


def test_status_metric(client):
    intervals_schema = {
        "type": "array",
        "items": {
            "type": "object",
            "properties": {
                "start": {"type": "string"},
                "end": {"type": "string"},
                "status": {"type": "string"},
            },
            "required": ["start", "end", "status"],
        },
    }
    schema = {
        "type": "object",
        "properties": {
            "raw": intervals_schema,
            "unit": intervals_schema,
            "final": intervals_schema,
        },
        "required": ["raw", "unit", "final"],
    }

    # processing
    response = client.get(
        "/v1/workflow/status/processing?location=torrentdepareis&start=2020-01-01T00:00:00&end=2020-03-01T00:00:00"
    )
    assert response.status_code == 200
    validate(response.json, schema)

    # validation
    response = client.get(
        "/v1/workflow/status/validation?location=torrentdepareis&start=2020-01-01T00:00:00&end=2020-03-01T00:00:00"
    )
    assert response.status_code == 200
    validate(response.json, schema)


def test_status_metric_incorrect_location(client):
    response = client.get(
        "/v1/workflow/status/processing?location=nonexistent&start=2020-01-01T00:00:00&end=2020-03-01T00:00:00"
    )
    assert response.status_code == 400
    assert "Location does not exists" in response.json.get("detail", {}).get("location")


def test_workflow_validate(client):
    expected_interval_nonvalidated = {
        "end": "2020-02-01T00:00:00+00:00",
        "start": "2020-01-01T00:00:00+00:00",
        "status": "nonvalidated",
    }
    expected_interval_validated = {
        "end": "2020-02-01T00:00:00+00:00",
        "start": "2020-01-01T00:00:00+00:00",
        "status": "validated",
    }
    expected_nonvalidated = {
        "final": [expected_interval_nonvalidated],
        "raw": [expected_interval_nonvalidated],
        "unit": [expected_interval_nonvalidated],
    }
    expected_validated_raw = {
        "final": [expected_interval_nonvalidated],
        "raw": [expected_interval_validated],
        "unit": [expected_interval_nonvalidated],
    }

    response = client.get(
        "/v1/workflow/status/validation?location=torrentdepareis&start=2020-01-01T00:00:00&end=2020-02-01T00:00:00"
    )
    assert response.status_code == 200
    assert response.json == expected_nonvalidated

    response = client.post(
        "/v1/workflow/validation",
        json=[
            {
                "location": "torrentdepareis",
                "start": "2020-01-01T00:00:00",
                "end": "2020-02-01T00:00:00",
                "step": "raw",
                "metric": metric,
            }
            for metric in ["temp10", "hum10", "ws10"]
        ],
    )
    assert response.status_code == 200
    assert response.json == {"success": True}

    response = client.get(
        "/v1/workflow/status/validation?location=torrentdepareis&start=2020-01-01T00:00:00&end=2020-02-01T00:00:00"
    )
    assert response.status_code == 200
    assert response.json == expected_validated_raw


@pytest.mark.parametrize(
    "incorrect_param",
    [
        {"location": "nonexistent"},
        {"start": "in-cor-ect da:te"},
        {"end": "in-cor-ect da:te"},
        {"step": "nonexistent"},
        {"metric": "nonexistent"},
    ],
)
def test_workflow_modify_incorrect(incorrect_param, client):
    params = {
        "location": "torrentdepareis",
        "start": "2020-01-01T00:00:00",
        "end": "2020-02-01T00:00:00",
        "step": "raw",
        "metric": "temp10",
    }

    params.update(incorrect_param)

    response = client.post("/v1/workflow/validation", json=[params])
    assert response.status_code == 400
