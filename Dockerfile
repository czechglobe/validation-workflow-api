ARG VALIDATION_WORKFLOW_VERSION

FROM registry.gitlab.com/czechglobe/validation-workflow-dags:${VALIDATION_WORKFLOW_VERSION}

COPY . ${AIRFLOW_HOME}/validation-workflow-api

USER root

RUN pip install -r ${AIRFLOW_HOME}/validation-workflow-api/all-requirements.txt

# this is used locally in docker-compose, cause /tmp doesn't work
RUN mkdir -p /gun-tmp && chown -R airflow: /gun-tmp && chmod 755 /gun-tmp

USER airflow

WORKDIR ${AIRFLOW_HOME}/validation-workflow-api
ENV PYTHONPATH=${AIRFLOW_HOME}/validation-workflow-dags:${AIRFLOW_HOME}/validation-workflow-api

EXPOSE 6542
CMD [ "gunicorn", "validation_workflow_api.app", "--config", "gunicorn_config.py"]
