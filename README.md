# validation-workflow-api

Part of Validation Workflow system (https://gitlab.com/czechglobe/validation-workflow). This submodule contains API which provides data for user interface and executes computations in airflow.
