"""Link to docs: http://docs.gunicorn.org/en/stable/settings.html"""

import os


workers = os.getenv("WEB_CONCURRENCY", "1")
proc_name = "validation-worflow-api"

bind = ":6542"

accesslog = "-"  # send access log to stdout

timeout = 180
